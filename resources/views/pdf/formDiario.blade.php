<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Reporte de Visitas</title>
</head>

<body >


@foreach($data as $value)
<div style="width:700px;  height: 900px;">
	<h1 align="center" ><strong>Reporte de actividad:</strong></h1>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000">
		<tr>
			<th style="height: auto">
				<p align="right"  ><strong>Asesor:</strong></p>
			</th>
			<th width=" 40%"  style=" border: black 2px solid;">
				<p align="center;" ><strong>{!! ($value->users)?$value->users->username:"Pendiente de Asignar" !!}</strong></p>
			</th>
		</tr>
		<tr nowrap height="auto">
			<th>
				<p align="right"  ><strong>Fecha:</strong></p>
			</th>
			<th width=" 40%"  style=" border: black 2px solid;">
				<p align="center;"><strong>{!! ($value->fecha_aut)?$value->fecha_aut:"Pendiente de Asignar" !!}</strong></p>
			</th>
		</tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000">
		<tr>
			<th >
				<p align="right"  ><strong>Actividad:</strong></p>
			</th>
			<th width=" 70%"  style=" border: black 2px solid;">
				<p align="center;" ><strong>Ventas y promocion</strong></p>
			</th>
		</tr>
		<tr>
			<th >
				<p align="right"  ><strong></strong></p>
			</th>
			<th width=" 70%"  style=" border: black 2px solid;">
				<p align="center;" ><strong>{!! ($value->tipos)?$value->tipos->nombre:"Pendiente de Asignar" !!}</strong></p>
			</th>
		</tr>
		<tr >
			<th>
				<p align="right"  ><strong>Cliente:</strong></p>
			</th>
			<th width=" 70%"  style=" border: black 2px solid;">
				<p align="center;"><strong>{!! ($value->clientes)?$value->clientes->nombre:"Pendiente de Asignar" !!} {!! ($value->clientes)?$value->clientes->apellido:"Pendiente de Asignar" !!}</strong></p>
			</th>
		</tr>
	</table>
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#000000">
		<tr width=" 100%">
			<th  width=" 30%" align="right">Depto: 
			</th>	
			<th width=" 25%" style=" border: black 2px solid;">
				{!! ($value->departamento)?$value->departamento:"Pendiente de Asignar" !!}
			</th>
			<th width=" 20%" style=" border: black 2px solid;" >
				<strong>Municipio</strong>
			</th>
			<th width=" 25%" style=" border: black 2px solid;">
				{!! ($value->municipio)?$value->municipio:"Pendiente de Asignar" !!}
			</th>
		</tr>
		<tr> 
			<th align="right">Objetivo:</th>
			<th colspan="3" style=" border: black 2px solid;"><p align="center;"><strong>{!! ($value->objetivos)?$value->objetivos->descripcion:"Pendiente de Asignar" !!}</strong></p></th>
		</tr>
		<tr width=" 100%">
			<th  width=" 30%" align="right">Venta Efectiva: 
			</th>	
			<th width=" 25%" style=" border: black 2px solid;">
				{!! ($value->venta_efectiva)?$value->venta_efectiva:"Pendiente de Asignar" !!}
			</th>
			<th width=" 20%" style=" border: black 2px solid;" >
				<strong>Seguimiento:</strong></th>
			<th width=" 25%" style=" border: black 2px solid;">
				{!! ($value->regreso)?$value->regreso:"" !!}
			</th>
		</tr>
		<tr> 
			<th align="right">Observaciones:</th>
			<th colspan="3" style=" border: black 2px solid;"><p align="center;"><strong>{!! ($value->observaciones)?$value->observaciones:"" !!}</strong></p></th>
		</tr>
		<tr> 
			<th align="right">Seguimiento:</th>
			<th colspan="3" style=" border: black 2px solid;"><p align="center;"><strong>{!! ($value->porque_regresar)?$value->porque_regresar:"Pendiente de Asignar" !!}</strong></p></th>
		</tr>
		
	</table>
	<table>
	<tr> 
			<th style="text-align:center;padding-left:5%" >
				<img style="margin-top:10px;;max-height:300px;width:30%;height:auto;" src="{!! ($value->foto1)?$value->foto1:'https://im.ziffdavisinternational.com/ign_es/screenshot/l/las-mejores-armas-de-link-en-the-legend-of-zelda/las-mejores-armas-de-link-en-the-legend-of-zelda_jg75.jpg' !!}">
			</th>
			<th style="text-align:center;" >
				<img style="margin-top:10px;max-height:300px;width:30%;height:auto;" src="{!! ($value->foto2)?$value->foto2:'https://im.ziffdavisinternational.com/ign_es/screenshot/l/las-mejores-armas-de-link-en-the-legend-of-zelda/las-mejores-armas-de-link-en-the-legend-of-zelda_jg75.jpg' !!}">
			</th>
			<th style="text-align:center;" >
				<img style="margin-top:10px;max-height:300px;width:30%;height:auto;" src="{!! ($value->foto3)?$value->foto3:'https://im.ziffdavisinternational.com/ign_es/screenshot/l/las-mejores-armas-de-link-en-the-legend-of-zelda/las-mejores-armas-de-link-en-the-legend-of-zelda_jg75.jpg' !!}">
			</th>
		</tr>
		</table>
</div>
@endforeach

</body>
</html>
