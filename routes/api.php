<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('accesos', 'AccesosController');
Route::resource('clientes', 'ClientesController');
Route::resource('clienteusuario', 'ClienteUsuarioController');
Route::resource('clientecamion', 'ClienteCamionesController');
Route::resource('comisiones', 'ComisionesController');
Route::resource('compras', 'ComprasController');
Route::resource('comprasdetalle', 'ComprasDetalleController');
Route::resource('correos', 'CorreosController');
Route::resource('cuentascobrar', 'CuentasCobrarController');
Route::resource('cuentaspagar', 'CuentasPagarController');
Route::resource('cultivos', 'CultivosController');
Route::resource('departamentos', 'DepartamentosController');
Route::resource('empleados', 'EmpleadosController');
Route::resource('gastos', 'GastosController');
Route::resource('inventario', 'InventarioController');
Route::resource('marcas', 'MarcasController');
Route::resource('modulos', 'ModulosController');
Route::resource('movimientosc', 'MovimientosCController');
Route::resource('movimientosp', 'MovimientosPController');
Route::resource('municipios', 'MunicipiosController');
Route::resource('paises', 'PaisesController');
Route::resource('productos', 'ProductosController');
Route::resource('proveedores', 'ProveedoresController');
Route::resource('puestos', 'PuestosController');
Route::resource('objetivos', 'ObjetivosController');
Route::resource('roles', 'RolesController');
Route::resource('sectores', 'SectoresController');
Route::resource('sucursales', 'SucursalesController');
Route::resource('sueldos', 'SueldosController');
Route::resource('tiposcompra', 'TiposCompraController');
Route::resource('tiposproducto', 'TiposProductoController');
Route::resource('tiposventa', 'TiposVentaController');
Route::resource('tiposdetallecompras', 'TiposDetalleComprasController');
Route::resource('tiposdetalleventas', 'TiposDetalleVentasController');
Route::resource('rutascamiones', 'RutasCamionesController');
Route::resource('rutas', 'RutasController');
Route::resource('usuarios', 'UsuariosController');
Route::resource('ubicaciones', 'UbicacionesController');
Route::resource('ventas', 'VentasController');
Route::resource('ventasdetalle', 'VentasDetalleController');

Route::resource('camiones', 'CamionesController');
Route::resource('inventariocamiones', 'InventarioCamionesControlles');
Route::resource('empleadoscamiones', 'EmpleadosCamionesControlles');
Route::resource('pedidos', 'PedidosController');
Route::resource('visitas', 'VisitasController');
Route::resource('tipovisita', 'TipoVisitaController');
Route::resource('tipocliente', 'TipoClienteController');
Route::resource('tipoavance', 'TipoAvanceController');
Route::resource('formdiario', 'FormDiarioController');
Route::resource('formvisita', 'FormVisitaController');
Route::post('all/empleadoscamiones', 'EmpleadosCamionesControlles@storeAll');

Route::get('usuarios/{id}/ubicaciones', 'UbicacionesController@getThisByUser');
Route::get('filter/usuarios/{id}/ubicaciones', 'UbicacionesController@getThisByFilter');
Route::get('filter/paises/{state}', 'PaisesController@getThisByFilter');
Route::get('usuarios/{id}/visitas', 'VisitasController@getThisByUser');
Route::get('clientes/{id}/visitas', 'VisitasController@getThisByClient');
Route::get('usuarios/{id}/clientes', 'ClienteUsuarioController@getThisByUser');
Route::get('free/clientes', 'ClienteUsuarioController@getThisByFree');
Route::get('free/usuarios', 'EmpleadosCamionesControlles@getThisByFree');
Route::post('clienteusuario/signeddown', 'ClienteUsuarioController@removeClientes');

Route::get('clientes/{id}/formdiario/{state}', 'FormDiarioController@getThisByFilter');
Route::get('clientes/{id}/formvisita/{state}', 'FormVisitaController@getThisByFilter');
Route::get('filter/{id}/sectores/{state}', 'SectoresController@getThisByFilter');
Route::get('pais/{id}/sectores', 'SectoresController@getThisByPais');

Route::get('clientes/{id}/formdiario', 'FormDiarioController@getThisByClient');
Route::get('clientes/{id}/formvisita', 'FormVisitaController@getThisByClient');
Route::get('clientes/{id}/rutas', 'RutasController@getThisByClient');

Route::get('usuarios/{id}/rutas', 'RutasController@getThisByUser');
Route::get('camiones/{id}/rutas', 'RutasCamionesController@getThisByUser');
Route::get('camiones/{id}/empleados', 'EmpleadosCamionesControlles@getThisByCamion');
Route::get('usuarios/{id}/formdiario', 'FormDiarioController@getThisByUser');
Route::get('usuarios/{id}/formvisita', 'FormVisitaController@getThisByUser');

Route::get('near/{id}/clientes', 'ClientesController@clientsNear');
Route::get('nearr/clientes', 'ClientesController@clientsNearAll');

Route::post('upload/{id}/usuarios', 'UsuariosController@uploadAvatarGeneral');

Route::get('report/formvisita', 'FormVisitaController@report');
Route::get('report/formdiario', 'FormDiarioController@report');
Route::get('report/logradodiario', 'FormDiarioController@logrado');
Route::get('report/logradovisita', 'FormVisitaController@logrado');

Route::get('report/empleados/logradovisita', 'FormVisitaController@logradoByEmpleados');
Route::get('report/empleados/logradodiario', 'FormDiarioController@logradoByEmpleados');

Route::get('report/logradodiario/{id}', 'FormDiarioController@logradoMine');
Route::get('report/logradovisita/{id}', 'FormVisitaController@logradoMine');

Route::get('report/formvisita/{id}', 'FormVisitaController@reportFilter');
Route::get('report/formdiario/{id}', 'FormDiarioController@reportFilter');

Route::post('usuarios/{id}/upload/avatar', 'UsuariosController@uploadAvatar');
Route::post('usuarios/{id}/changepassword', 'UsuariosController@changePassword');
Route::post('usuarios/password/reset', 'UsuariosController@recoveryPassword');

Route::get('usuarios/{id}/modulos', 'AccesosController@getAccesos');
Route::get('usuarios/{id}/modulos/{id2}', 'AccesosController@getAcceso');
Route::get('anuladas/compras', 'ComprasController@anuladas');
Route::get('anuladas/ventas', 'VentasController@anuladas');
Route::get('buscar/proveedores', 'ProveedoresController@find');
Route::get('buscar/clientes', 'ClientesController@find');
Route::get('pagadas/cuentaspagar', 'CuentasPagarController@pagadas');
Route::get('pagadas/cuentascobrar', 'CuentasCobrarController@pagadas');
Route::get('existencia/productos', 'ProductosController@existencia');
Route::get('admin/inventario', 'InventarioController@admin');
Route::get('comprobante/ventas', 'VentasController@comprobante');
Route::get('ventas/by/clientes/{id}', 'VentasController@ventasByClient');
Route::get('cuentascobrar/by/clientes/{id}', 'CuentasCobrarController@cuentasByClient');

Route::get('vendedores/estadistica/barra', 'VentasController@estadisticaVendedoresBarra');
Route::get('clientes/estadistica/barra', 'VentasController@estadisticaClientesBarra');
Route::get('ventas/estadistica/barra', 'VentasController@estadisticaVentasBarra');
Route::get('ventas/estadistica/pie', 'VentasController@estadisticaVentasPie');
Route::get('clientes/estadistica/pie', 'VentasController@estadisticaClientesPie');
Route::get('vendedores/estadistica/pie', 'VentasController@estadisticaVendedoresPie');

Route::post('login', 'AuthenticateController@login');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
