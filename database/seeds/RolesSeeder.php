<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_cliente')->insert([
            'descripcion'      => "AGROSERVICIO",
            'nombre'           => "AGROSERVICIO",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_cliente')->insert([
            'descripcion'      => "DISTRIBUIDOR",
            'nombre'           => "DISTRIBUIDOR",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_cliente')->insert([
            'descripcion'      => "MAYORISTA",
            'nombre'           => "MAYORISTA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_cliente')->insert([
            'descripcion'      => "FINCA",
            'nombre'           => "FINCA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_cliente')->insert([
            'descripcion'      => "COOPERATIVA",
            'nombre'           => "COOPERATIVA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_cliente')->insert([
            'descripcion'      => "AGROEXPORTADORA",
            'nombre'           => "AGROEXPORTADORA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_cliente')->insert([
            'descripcion'      => "AGRICULTOR",
            'nombre'           => "AGRICULTOR",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "PARCELAS",
            'nombre'           => "PARCELAS",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "VENTAS",
            'nombre'           => "VENTAS",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "APOYO A MOSTRADOR",
            'nombre'           => "APOYO A MOSTRADOR",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "PAGO DE FACTURAS",
            'nombre'           => "PAGO DE FACTURAS",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "COBRO DE FACTURAS",
            'nombre'           => "COBRO DE FACTURAS",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "CHARLAS TECNICAS",
            'nombre'           => "CHARLAS TECNICAS",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "DIAS DE CAMPO",
            'nombre'           => "DIAS DE CAMPO",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "REUNION DE VENTAS",
            'nombre'           => "REUNION DE VENTAS",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "EVALUACIONES",
            'nombre'           => "EVALUACIONES",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "SEGUIMIENTO A RECLAMOS",
            'nombre'           => "SEGUIMIENTO A RECLAMOS",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "ENTREGA DE PRODUCTOS",
            'nombre'           => "ENTREGA DE PRODUCTOS",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('objetivos')->insert([
            'descripcion'      => "VISITA AGROSERVICIO",
            'nombre'           => "VISITA AGROSERVICIO",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "CAFÉ",
            'nombre'           => "CAFÉ",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "MAIZ",
            'nombre'           => "MAIZ",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "FRIJOL",
            'nombre'           => "FRIJOL",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "CAÑA",
            'nombre'           => "CAÑA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "TOMATE Y CHILE",
            'nombre'           => "TOMATE Y CHILE",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "PAPA",
            'nombre'           => "PAPA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "ARVEJA Y EJOTE",
            'nombre'           => "ARVEJA Y EJOTE",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "CEBOLLA",
            'nombre'           => "CEBOLLA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "MELON",
            'nombre'           => "MELON",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "SANDIA",
            'nombre'           => "SANDIA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "PALMA AFRICANA",
            'nombre'           => "PALMA AFRICANA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "PEPINO",
            'nombre'           => "PEPINO",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "LIMON",
            'nombre'           => "LIMON",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "ORNAMENTALES",
            'nombre'           => "ORNAMENTALES",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "MACADAMIA",
            'nombre'           => "MACADAMIA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "BANANO",
            'nombre'           => "BANANO",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "PLATANO",
            'nombre'           => "PLATANO",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "PIÑA",
            'nombre'           => "PIÑA",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "TABACO",
            'nombre'           => "TABACO",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "HULE",
            'nombre'           => "HULE",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "MANI",
            'nombre'           => "MANI",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('cultivos')->insert([
            'descripcion'      => "ARROZ",
            'nombre'           => "ARROZ",
            'estado'           => 1,
            'codigo'           => null,
            'tipo'             => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        
        // DB::table('roles')->insert([
        //     'id'               => 1,
        //     'descripcion'      => "Administrador",
        //     'modulos'          => "123456",
        //     'estado'           => 1,
        //     'deleted_at'       => null,
        //     'created_at'       => date('Y-m-d H:m:s'),
        //     'updated_at'       => date('Y-m-d H:m:s')
        // ]);
        
        // DB::table('roles')->insert([
        //     'id'               => 2,
        //     'descripcion'      => "Soporte",
        //     'modulos'          => "123456",
        //     'estado'           => 0,
        //     'deleted_at'       => null,
        //     'created_at'       => date('Y-m-d H:m:s'),
        //     'updated_at'       => date('Y-m-d H:m:s')
        // ]);

        // DB::table('roles')->insert([
        //     'id'               => 3,
        //     'descripcion'      => "Usuario",
        //     'modulos'          => "123456",
        //     'estado'           => 1,
        //     'deleted_at'       => null,
        //     'created_at'       => date('Y-m-d H:m:s'),
        //     'updated_at'       => date('Y-m-d H:m:s')
        // ]);
    }
}
