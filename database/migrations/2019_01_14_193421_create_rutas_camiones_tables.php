<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRutasCamionesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rutas_camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha')->nullable()->default(null);
            $table->string('comprobante')->nullable()->default(1);
            $table->tinyInteger('tipo')->nullable()->default(1);
            $table->tinyInteger('estado')->nullable()->default(1);
            $table->tinyInteger('completado')->nullable()->default(1);

            $table->double('latitud_pre',15,8)->nullable()->default(null);
            $table->double('latitud_fin',15,8)->nullable()->default(null);
            $table->double('longitud_fin',15,8)->nullable()->default(null);
            $table->double('longitud_pre',15,8)->nullable()->default(null);
            $table->timestamp('fecha_aut')->current();

            $table->integer('clientes_camione')->unsigned()->nullable()->default(null);
            $table->foreign('clientes_camione')->references('id')->on('clientes_camiones')->onDelete('cascade');

            $table->integer('camion')->unsigned()->nullable()->default(null);
            $table->foreign('camion')->references('id')->on('camiones')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rutas_camiones');
    }
}
