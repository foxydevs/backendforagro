<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormDiarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_diario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable()->default(null);
            $table->string('ubicacion')->nullable()->default(null);
            $table->string('regresar')->nullable()->default(null);
            $table->string('porque_regresar')->nullable()->default(null);
            $table->string('observaciones')->nullable()->default(null);
            $table->string('foto1')->nullable()->default(null);
            $table->string('foto2')->nullable()->default(null);
            $table->string('foto3')->nullable()->default(null);
            $table->string('video')->nullable()->default(null);
            $table->integer('logrado')->nullable()->default(null);
            $table->string('razon')->nullable()->default(null);
            $table->double('latitud',15,8)->nullable()->default(null);
            $table->double('longitud',15,8)->nullable()->default(null);

            $table->integer('planificacion')->unsigned()->nullable()->default(null);
            $table->foreign('planificacion')->references('id')->on('form_diario')->onDelete('cascade');
           
            $table->timestamp('fecha_aut')->useCurrent();

            $table->string('venta_efectiva')->nullable()->default(null);
            $table->string('regreso')->nullable()->default(null);
           
            $table->integer('tipo')->nullable()->default(null);
            $table->integer('estado')->nullable()->default(1);

            $table->integer('cliente')->unsigned()->nullable()->default(null);
            $table->foreign('cliente')->references('id')->on('clientes')->onDelete('cascade');

            $table->integer('usuario')->unsigned()->nullable()->default(null);
            $table->foreign('usuario')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('tipo_visita')->unsigned()->nullable()->default(null);
            $table->foreign('tipo_visita')->references('id')->on('tipo_visita')->onDelete('cascade');

            $table->integer('objetivo')->unsigned()->nullable()->default(null);
            $table->foreign('objetivo')->references('id')->on('objetivos')->onDelete('cascade');

            $table->integer('tipo_cultivo')->unsigned()->nullable()->default(null);
            $table->foreign('tipo_cultivo')->references('id')->on('cultivos')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_diario');
    }
}
