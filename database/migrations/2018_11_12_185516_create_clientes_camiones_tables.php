<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesCamionesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes_camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha')->nullable()->default(null);
            $table->string('comprobante')->nullable()->default(1);
            $table->tinyInteger('tipo')->nullable()->default(1);
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->double('latitud',15,8)->nullable()->default(null);
            $table->double('longitud',15,8)->nullable()->default(null);
            $table->timestamp('fecha_aut')->current();

            $table->integer('cliente')->unsigned()->nullable()->default(null);
            $table->foreign('cliente')->references('id')->on('clientes')->onDelete('cascade');

            $table->integer('camion')->unsigned()->nullable()->default(null);
            $table->foreign('camion')->references('id')->on('camiones')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes_camiones');
    }
}
