<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbicacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubicaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('fecha_aut')->useCurrent();
            $table->date('fecha')->nullable()->default(null);
            $table->time('hora')->nullable()->default(null);
            $table->string('place_id')->nullable()->default(null);
            $table->string('place')->nullable()->default(null);
            $table->string('direccion')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->double('latitude',15,8)->nullable()->default(null);
            $table->double('longitude',15,8)->nullable()->default(null);
            $table->integer('tipo')->nullable()->default(null);
            $table->integer('state')->nullable()->default(null);

            $table->integer('usuario')->nullable()->default(null)->unsigned();
            $table->foreign('usuario')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('camion')->nullable()->default(null)->unsigned();
            $table->foreign('camion')->references('id')->on('camiones')->onDelete('cascade');

            $table->integer('empleados_camion')->nullable()->default(null)->unsigned();
            $table->foreign('empleados_camion')->references('id')->on('empleados_camiones')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ubicaciones');
    }
}
