<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCamionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable()->default(null);
            $table->string('codigo')->nullable()->default(null);
            $table->string('placa')->nullable()->default(null);
            $table->string('modelo')->nullable()->default(null);
            $table->string('marca')->nullable()->default(null);
            $table->date('anio')->nullable()->default(null);
           
            $table->string('placas')->nullable()->default(null);	
            $table->string('unidad')->nullable()->default(null);	
            $table->string('fecha')->nullable()->default(null);	
            $table->string('mes')->nullable()->default(null);	
            $table->string('año')->nullable()->default(null);	
            $table->string('km_recorridos')->nullable()->default(null);	
            $table->string('klt_entregados_clientes')->nullable()->default(null);	
            $table->string('klt_no_entregados')->nullable()->default(null);	
            $table->string('klt_planta/zf/regionales')->nullable()->default(null);	
            $table->string('unidades_entregadas')->nullable()->default(null);	
            $table->string('%_carga')->nullable()->default(null);	
            $table->string('galones_de_diesel')->nullable()->default(null);	
            $table->string('diesel')->nullable()->default(null);	
            $table->string('renta_/_mensualidad_leasing')->nullable()->default(null);	
            $table->string('costo_por_km_adicional')->nullable()->default(null);	
            $table->string('piloto/ayudante_salarios_(incluye_prest)_promedio_diario')->nullable()->default(null);	
            $table->string('kilometraje_piloto')->nullable()->default(null);	
            $table->string('seguridad')->nullable()->default(null);	
            $table->string('viáticos')->nullable()->default(null);	
            $table->string('seguros')->nullable()->default(null);	
            $table->string('mantenimientos_preventivos')->nullable()->default(null);	
            $table->string('mantenimientos_correctivos')->nullable()->default(null);	
            $table->string('llantas')->nullable()->default(null);	
            $table->string('impuesto')->nullable()->default(null);	
            $table->string('gps')->nullable()->default(null);	
            $table->string('total_de_gastos')->nullable()->default(null);	
            $table->string('costo_kilometro_recorrido')->nullable()->default(null);	
            $table->string('costo_kilo/litro_entregado')->nullable()->default(null);	
            $table->string('costo_promedio')->nullable()->default(null);	
            $table->string('observaciones')->nullable()->default(null);	
            $table->string('disponibilidad')->nullable()->default(null);	
            $table->string('uso')->nullable()->default(null);	
            $table->string('cantidad_de_envios_transportados')->nullable()->default(null);	
            $table->string('cantidad_de_envios_entregados')->nullable()->default(null);	
            $table->string('cantidad_de_envios_no_entregados')->nullable()->default(null);	
            $table->string('viajes')->nullable()->default(null);	
            $table->string('piloto')->nullable()->default(null);	
            $table->string('hora_de_salida')->nullable()->default(null);	
            $table->string('tiempo_de_trabajo')->nullable()->default(null);	
            $table->string('tiempo_efectivo_de_trabajo')->nullable()->default(null);	
            $table->string('tiempo_desperdiciado_(detenido_con_motor_encendido)')->nullable()->default(null);	
            $table->string('ruta')->nullable()->default(null);

            $table->integer('tipo')->nullable()->default(null);
            $table->integer('estado')->nullable()->default(1);
            $table->string('color')->nullable()->default(null);
            $table->string('colorbg')->nullable()->default(null);
            $table->string('imagen')->nullable()->default(null);
            $table->string('video')->nullable()->default(null);
            $table->string('round')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camiones');
    }
}
