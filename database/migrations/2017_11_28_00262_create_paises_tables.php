<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaisesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable()->default(null);
            $table->string('codigo')->nullable()->default(null);
            $table->string('LADA')->nullable()->default(null);
            $table->string('cod')->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->string('countryCode')->nullable()->default(null);	
            $table->string('countryName')->nullable()->default(null);	
            $table->string('postalCode')->nullable()->default(null);	
            $table->string('administrativeArea')->nullable()->default(null);	
            $table->string('subAdministrativeArea')->nullable()->default(null);	
            $table->string('locality')->nullable()->default(null);	
            $table->string('subLocality')->nullable()->default(null);	
            $table->string('thoroughfare')->nullable()->default(null);	
            $table->string('subThoroughfare')->nullable()->default(null);	
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paises');
    }
}
