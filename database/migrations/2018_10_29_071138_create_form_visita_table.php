<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormVisitaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_visita', function (Blueprint $table) {
            $table->increments('id');
            $table->string('semana')->nullable()->default(null);
            $table->date('fecha')->nullable()->default(null);
            $table->time('hora')->nullable()->default(null);
            $table->string('nombre_cliente')->nullable()->default(null);
            $table->string('lugar')->nullable()->default(null);
            $table->string('departamento')->nullable()->default(null);
            $table->string('observaciones')->nullable()->default(null);
            $table->integer('logrado')->nullable()->default(null);
            $table->string('razon')->nullable()->default(null);
            $table->double('latitud',15,8)->nullable()->default(null);
            $table->double('longitud',15,8)->nullable()->default(null);
            $table->string('foto1')->nullable()->default(null);
            $table->string('foto2')->nullable()->default(null);
            $table->string('foto3')->nullable()->default(null);
            $table->string('video')->nullable()->default(null);
            $table->string('regresar')->nullable()->default(null);
            $table->string('porque_regresar')->nullable()->default(null);
            
            $table->timestamp('fecha_aut')->current();

            $table->integer('tipo')->nullable()->default(null);
            $table->integer('estado')->nullable()->default(1);

            $table->integer('planificacion')->unsigned()->nullable()->default(null);
            $table->foreign('planificacion')->references('id')->on('form_visita')->onDelete('cascade');

            $table->integer('usuario')->unsigned()->nullable()->default(null);
            $table->foreign('usuario')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('cliente')->unsigned()->nullable()->default(null);
            $table->foreign('cliente')->references('id')->on('clientes')->onDelete('cascade');

            $table->integer('tipo_cliente')->unsigned()->nullable()->default(null);
            $table->foreign('tipo_cliente')->references('id')->on('tipo_cliente')->onDelete('cascade');

            $table->integer('tipo_avance')->unsigned()->nullable()->default(null);
            $table->foreign('tipo_avance')->references('id')->on('tipo_avance')->onDelete('cascade');

            $table->integer('objetivos')->unsigned()->nullable()->default(null);
            $table->foreign('objetivos')->references('id')->on('objetivos')->onDelete('cascade');

            $table->integer('cultivo')->unsigned()->nullable()->default(null);
            $table->foreign('cultivo')->references('id')->on('cultivos')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_visita');
    }
}
