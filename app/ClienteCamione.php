<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteCamione extends Model
{
    protected $table = 'clientes_camiones';

    public function users(){
        return $this->hasOne('App\Usuarios','id','usuario');
    }
}
