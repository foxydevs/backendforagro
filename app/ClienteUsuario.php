<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteUsuario extends Model
{
    protected $table = 'cliente_usuario';

    public function users(){
        return $this->hasOne('App\Usuarios','id','usuario');
    }
}
