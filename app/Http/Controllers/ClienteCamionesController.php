<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ClienteCamiones;
use App\Clientes;
use Response;
use DB;
use Validator;

class ClienteCamionesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(ClienteCamiones::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = ClienteCamiones::whereRaw('camion=? and estado=?',[$id,$state])->with('users')->get();
                    break;
                }
                case 'type':{
                    $objectSee = ClienteCamiones::whereRaw('camion=? and tipo=?',[$id,$state])->with('users')->get();
                    break;
                }
                default:{
                    $objectSee = ClienteCamiones::whereRaw('camion=? and estado=?',[$id,$state])->with('users')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = ClienteCamiones::whereRaw('camion=? and estado=?',[$id,$state])->with('users')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = ClienteCamiones::select('cliente')->where('camion','=',$id)->get();
        if ($objectSee) {
            $objectSee1 = Clientes::whereIn('id',$objectSee)->get();
    
            return Response::json($objectSee1, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getThisByFree()
    {
        $objectSee = ClienteCamiones::select('cliente')->get();
        if ($objectSee) {
            $objectSee1 = Clientes::whereNotIn('id',$objectSee)->get();
    
            return Response::json($objectSee1, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = ClienteCamiones::where('cliente','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientes'          => 'required',
            'camion'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                if ( $request->get('clientes') )
                {
                    DB::beginTransaction();
                    $Array = $request->get('clientes');
                    $tutor = $request->get('camion');
                    foreach ($Array as $value)
                    {
                        $existe = ClienteCamiones::whereRaw('cliente=? and camion=?',[$value['id'],$tutor])->first();
                        if(!($existe)){   
                            $objectSee = Clientes::find($value['id']);
                            if ($objectSee) {
                                $newObject = new ClienteCamiones();
                                $newObject->comprobante            = $request->get('comprobante',1);
                                $newObject->fecha            = $request->get('fecha',null);
                                $newObject->estado            = $request->get('estado',2);
                                $newObject->latitud            = $objectSee->latitud;
                                $newObject->longitud            = $objectSee->longitud;
                                $newObject->tipo            = $request->get('tipo',1);
                                $newObject->cliente            = $value['id'];
                                $newObject->camion            = $tutor;
                                $newObject->save();
                            }
                            else {
                                $returnData = array (
                                    'status' => 404,
                                    'message' => 'No record found'
                                );
                                return Response::json($returnData, 404);
                            } 
                            
                        }
                    }
                    DB::commit();
                    $returnData = array (
                        'status' => 200,
                        'message' => "success"
                    );
                    return Response::json($returnData, 200);
                }

                $returnData = array (
                    'status' => 200,
                    'message' => "success"
                );
                return Response::json($returnData, 200);
                
                
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = ClienteCamiones::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    public function removeClientes(Request $request)
    {
        try
        {
            if ( $request->get('clientes') )
            {
                DB::beginTransaction();
                $Array = $request->get('clientes');
                $tutor = $request->get('camion');
                $studentsId = collect();
                foreach ($Array as $value)
                {
                    $objectDelete = ClienteCamiones::whereRaw('cliente=? and camion=?',[$value['id'],$tutor])->first();
                    if(($objectDelete)){    
                        $studentsId->push($objectDelete->id); 
                        ClienteCamiones::destroy($objectDelete->id);      
                    } 
                }

                DB::commit();
                $returnData = array (
                    'status' => 200,
                    'message' => "success"
                );
                return Response::json($returnData, 200);
            }
            $returnData = array (
                'status' => 200,
                'message' => "success"
            );
            return Response::json($returnData, 200);  
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            if($e->errorInfo[0] == '01000'){
                $errorMessage = "Error Constraint";
            }  else {
                $errorMessage = $e->getMessage();
            }
            $returnData = array (
                'status' => 505,
                'SQLState' => $e->errorInfo[0],
                'message' => $errorMessage
            );
            return Response::json($returnData, 500);
        }
        catch (Exception $e)
        {
            DB::rollback();
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = ClienteCamiones::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->comprobante            = $request->get('comprobante', $objectUpdate->comprobante);
                $objectUpdate->fecha            = $request->get('fecha', $objectUpdate->fecha);
                $objectUpdate->estado            = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->latitud            = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->longitud            = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->tipo            = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->cliente            = $request->get('cliente', $objectUpdate->cliente);
                $objectUpdate->camion            = $request->get('camion', $objectUpdate->camion);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = ClienteCamiones::find($id);
        if ($objectDelete) {
            try {
                ClienteCamiones::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
