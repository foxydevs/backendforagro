<?php

namespace App\Http\Controllers;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\FormVisita;
use PDF;
use Response;
use Validator;
use DB;

class FormVisitaController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(FormVisita::with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get(), 200);
    }

    public function logrado(){
        $objectSee = FormVisita::select(DB::raw('count(*) as cantidad, logrado'))->groupby('logrado')->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function logradoByEmpleados(Request $request){
        if($request->get('labels')){
            $objectSee = FormVisita::select(DB::raw('count(*) as cantidad, logrado, usuario'))->groupby('usuario')->orderby('usuario','asc')->orderby('logrado','asc')->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();

        }else{
            $objectSee = FormVisita::select(DB::raw('count(*) as cantidad, logrado, usuario'))->groupby('logrado','usuario')->orderby('usuario','asc')->orderby('logrado','asc')->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();
        }
        
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function logradoMine($id){
        $objectSee = FormVisita::select(DB::raw('count(*) as cantidad, logrado'))->whereRaw('usuario=?',$id)->groupby('logrado')->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function report()
    {
        $objectSee = FormVisita::with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();

        if ($objectSee) {
            try {
                    Mail::send('pdf.formVisita', ["data" => $objectSee], function (Message $message) use ($objectSee){
                        $message->from('info@foxylabs.gt', 'Info Foxylabs')
                                ->sender('info@foxylabs.gt', 'Info Foxylabs')
                                ->to('rtrejo@gmail.com', "Foragro")
                                ->replyTo('info@foxylabs.gt', 'Info Foxylabs')
                                ->subject('Reporte');
                    
                    });
                // return Response::json($objectSee, 500);
                $viewPDF = view('pdf.formVisita', ["data" => $objectSee]);
                $pdf = PDF::loadHTML($viewPDF);
                return $pdf->stream('download.pdf');
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
            
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
        
    }
    
    public function reportFilter($id)
    {
        $objectSee = FormVisita::whereRaw('usuario=?',$id)->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();

        if ($objectSee) {
            try {
                // return Response::json($objectSee, 500);
                Mail::send('pdf.formVisita', ["data" => $objectSee], function (Message $message) use ($objectSee){
                    $message->from('info@foxylabs.gt', 'Info Foxylabs')
                            ->sender('info@foxylabs.gt', 'Info Foxylabs')
                            ->to('rtrejo@gmail.com', "Foragro")
                            ->replyTo('info@foxylabs.gt', 'Info Foxylabs')
                            ->subject('Reporte');
                
                });
                $viewPDF = view('pdf.formVisita', ["data" => $objectSee]);
                $pdf = PDF::loadHTML($viewPDF);
                return $pdf->stream('download.pdf');
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
            
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
        
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = FormVisita::whereRaw('cliente=? and estado=? and fecha=?',[$id,$state,$request->get('fecha')])->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();
                    break;
                }
                case 'type':{
                    $objectSee = FormVisita::whereRaw('cliente=? and tipo=? and fecha=?',[$id,$state,$request->get('fecha')])->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();
                    break;
                }
                default:{
                    $objectSee = FormVisita::whereRaw('cliente=? and estado=? and fecha=?',[$id,$state,$request->get('fecha')])->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = FormVisita::whereRaw('cliente=? and estado=? and fecha=?',[$id,$state,$request->get('fecha')])->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = FormVisita::where('usuario','=',$id)->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = FormVisita::where('cliente','=',$id)->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();
        if ($objectSee) {
            foreach ($objectSee as $key => $value) {
                $value['terminado'] = 0;
                if($value['logrado']){
                    if(strlen($value['resultados'])>0){
                        foreach ($value['resultados'] as $key1 => $value1) {
                            if($value1['logrado']){
                                $value['terminado'] = 1; 
                            }
                        }
                    }else {
                        $value['terminado'] = 1; 
                    }
                }
            }
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cliente'          => 'required',
            'tipo_avance'      => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new FormVisita();
                $newObject->semana            = $request->get('semana', null);
                $newObject->fecha            = $request->get('fecha', null);
                $newObject->hora            = $request->get('hora', null);
                $newObject->nombre_cliente            = $request->get('nombre_cliente', null);
                $newObject->lugar            = $request->get('lugar', null);
                $newObject->departamento            = $request->get('departamento', null);
                $newObject->objetivos            = $request->get('objetivos', null);
                $newObject->cultivo            = $request->get('cultivo', null);
                $newObject->observaciones            = $request->get('observaciones', null);
                $newObject->latitud            = $request->get('latitud', null);
                $newObject->longitud            = $request->get('longitud', null);
                $newObject->logrado            = $request->get('logrado', null);
                $newObject->razon            = $request->get('razon', null);
                $newObject->tipo            = $request->get('tipo', null);
                $newObject->estado            = $request->get('estado', null);
                $newObject->cliente            = $request->get('cliente', null);
                $newObject->planificacion            = $request->get('planificacion', null);
                $newObject->tipo_cliente            = $request->get('tipo_cliente', null);
                $newObject->tipo_avance            = $request->get('tipo_avance', null);
                $newObject->usuario            = $request->get('usuario', null);
                $newObject->foto1              = $request->get('foto1',null);
                $newObject->foto2              = $request->get('foto2',null);
                $newObject->foto3              = $request->get('foto3',null);
                $newObject->video              = $request->get('video',null);
                $newObject->regresar              = $request->get('regresar',null);
                $newObject->porque_regresar              = $request->get('porque_regresar',null);
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = FormVisita::whereRaw('id=?',$id)->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->first();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = FormVisita::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->semana            = $request->get('semana', $objectUpdate->semana);
                $objectUpdate->fecha            = $request->get('fecha', $objectUpdate->fecha);
                $objectUpdate->hora            = $request->get('hora', $objectUpdate->hora);
                $objectUpdate->nombre_cliente            = $request->get('nombre_cliente', $objectUpdate->nombre_cliente);
                $objectUpdate->lugar            = $request->get('lugar', $objectUpdate->lugar);
                $objectUpdate->departamento            = $request->get('departamento', $objectUpdate->departamento);
                $objectUpdate->objetivos            = $request->get('objetivos', $objectUpdate->objetivos);
                $objectUpdate->cultivo            = $request->get('cultivo', $objectUpdate->cultivo);
                $objectUpdate->observaciones            = $request->get('observaciones', $objectUpdate->observaciones);
                $objectUpdate->latitud            = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->longitud            = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->tipo            = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->estado            = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->logrado            = $request->get('logrado', $objectUpdate->logrado);
                $objectUpdate->razon            = $request->get('razon', $objectUpdate->razon);
                $objectUpdate->cliente            = $request->get('cliente', $objectUpdate->cliente);
                $objectUpdate->tipo_cliente            = $request->get('tipo_cliente', $objectUpdate->tipo_cliente);
                $objectUpdate->tipo_avance            = $request->get('tipo_avance', $objectUpdate->tipo_avance);
                $objectUpdate->usuario            = $request->get('usuario', $objectUpdate->usuario);
                $objectUpdate->foto1              = $request->get('foto1',$objectUpdate->foto1);
                $objectUpdate->foto2              = $request->get('foto2',$objectUpdate->foto2);
                $objectUpdate->foto3              = $request->get('foto3',$objectUpdate->foto3);
                $objectUpdate->video              = $request->get('video',$objectUpdate->video);
                $objectUpdate->regresar              = $request->get('regresar',$objectUpdate->regresar);
                $objectUpdate->porque_regresar              = $request->get('porque_regresar',$objectUpdate->porque_regresar);
                $objectUpdate->planificacion            = $request->get('planificacion', $objectUpdate->planificacion);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = FormVisita::find($id);
        if ($objectDelete) {
            try {
                FormVisita::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
