<?php

namespace App\Http\Controllers;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\FormDiario;
use Response;
use PDF;
use Validator;
use DB;

class FormDiarioController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(FormDiario::with('clientes','tipos','users','resultados','objetivos','cultivos')->get(), 200);
    }
    public function logrado(){
        $objectSee = FormDiario::select(DB::raw('count(*) as cantidad, logrado'))->groupby('logrado')->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function logradoMine($id){
        $objectSee = FormDiario::select(DB::raw('count(*) as cantidad, logrado'))->whereRaw('usuario=?',$id)->groupby('logrado')->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    

    public function logradoByEmpleados(Request $request){
        if($request->get('labels')){
            $objectSee = FormDiario::select(DB::raw('count(*) as cantidad, logrado, usuario'))->groupby('usuario')->orderby('usuario','asc')->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();

        }else{
            $objectSee = FormDiario::select(DB::raw('count(*) as cantidad, logrado, usuario'))->groupby('logrado','usuario')->orderby('usuario','asc')->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();
        }
        
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function report()
    {
        // $objectSee = FormDiario::with('clientes','tipos','users','resultados','objetivos','cultivos')->get();
        $objectSee = FormVisita::with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();

        if ($objectSee) {
            try {
                Mail::send('pdf.formDiario', ["data" => $objectSee], function (Message $message) use ($objectSee){
                    $message->from('info@foxylabs.gt', 'Info Foxylabs')
                            ->sender('info@foxylabs.gt', 'Info Foxylabs')
                            ->to('rtrejo@foragro.com', "Foragro")
                            ->replyTo('info@foxylabs.gt', 'Info Foxylabs')
                            ->subject('Reporte');
                
                });
                $viewPDF = view('pdf.formDiario', ["data" => $objectSee]);
                $pdf = PDF::loadHTML($viewPDF);
                return $pdf->stream('download.pdf');
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
            
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
        
    }
    
    public function reportFilter($id)
    {
        $objectSee = FormVisita::whereRaw('usuario=?',$id)->with('clientes','tiposCliente','tiposAvance','users','resultados','objetivo','cultivos')->get();
        // $objectSee = FormDiario::whereRaw('usuario=?',$id)->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();

        if ($objectSee) {
            try {
                Mail::send('pdf.formDiario', ["data" => $objectSee], function (Message $message) use ($objectSee){
                    $message->from('info@foxylabs.gt', 'Info Foxylabs')
                            ->sender('info@foxylabs.gt', 'Info Foxylabs')
                            ->to('rtrejo@foragro.com', "Foragro")
                            ->replyTo('info@foxylabs.gt', 'Info Foxylabs')
                            ->subject('Reporte');
                
                });
                // return Response::json($objectSee, 200);

                $viewPDF = view('pdf.formDiario', ["data" => $objectSee]);
                $pdf = PDF::loadHTML($viewPDF);
                return $pdf->stream('download.pdf');
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
            
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
        
    }

    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = FormDiario::whereRaw('cliente=? and estado=? and LEFT((fecha_aut),10)=?',[$id,$state,$request->get('fecha')])->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();
                    break;
                }
                case 'type':{
                    $objectSee = FormDiario::whereRaw('cliente=? and tipo=? and LEFT((fecha_aut),10)=?',[$id,$state,$request->get('fecha')])->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();
                    break;
                }
                default:{
                    $objectSee = FormDiario::whereRaw('cliente=? and estado=? and LEFT((fecha_aut),10)=?',[$id,$state,$request->get('fecha')])->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = FormDiario::whereRaw('cliente=? and estado=? and LEFT((fecha_aut),10)=?',[$id,$state,$request->get('fecha')])->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = FormDiario::where('usuario','=',$id)->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = FormDiario::where('cliente','=',$id)->with('clientes','tipos','users','resultados','objetivos','cultivos')->get();
        if ($objectSee) {
            foreach ($objectSee as $key => $value) {
                $value['terminado'] = 0;
                if($value['logrado']){
                    if(strlen($value['resultados'])>0){
                        foreach ($value['resultados'] as $key1 => $value1) {
                            if($value1['logrado']){
                                $value['terminado'] = 1; 
                            }
                        }
                    }else {
                        $value['terminado'] = 1; 
                    }
                }
            }
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cliente'          => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new FormDiario();
                $newObject->nombre            = $request->get('nombre', null);
                $newObject->ubicacion            = $request->get('ubicacion', null);
                $newObject->objetivo            = $request->get('objetivo', null);
                $newObject->tipo_cultivo            = $request->get('tipo_cultivo', null);
                $newObject->regresar            = $request->get('regresar', null);
                $newObject->porque_regresar            = $request->get('porque_regresar', null);
                $newObject->observaciones            = $request->get('observaciones', null);
                $newObject->foto1            = $request->get('foto1', null);
                $newObject->foto2            = $request->get('foto2', null);
                $newObject->foto3            = $request->get('foto3', null);
                $newObject->video            = $request->get('video', null);
                $newObject->latitud            = $request->get('latitud', null);
                $newObject->longitud            = $request->get('longitud', null);
                $newObject->logrado            = $request->get('logrado', null);
                $newObject->razon            = $request->get('razon', null);
                $newObject->venta_efectiva            = $request->get('venta_efectiva', null);
                $newObject->regreso            = $request->get('regreso', null);
                $newObject->tipo            = $request->get('tipo', null);
                $newObject->estado            = $request->get('estado', null);
                $newObject->cliente            = $request->get('cliente', null);
                $newObject->tipo_visita            = $request->get('tipo_visita', null);
                $newObject->planificacion            = $request->get('planificacion', null);
                $newObject->usuario            = $request->get('usuario', null);
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    public function uploadAvatar(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'avatar'      => 'required|image|mimes:jpeg,png,jpg'
        ]);
    
        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
    
                $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);
    
                $objectUpdate->foto1 = Storage::disk('s3')->url($path);
                $objectUpdate->save();
    
                return Response::json($objectUpdate, 200);
    
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
    
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = FormDiario::whereRaw('id=?',$id)->with('clientes','tipos','users','resultados','objetivos','cultivos')->first();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = FormDiario::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombre            = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->ubicacion            = $request->get('ubicacion', $objectUpdate->ubicacion);
                $objectUpdate->objetivo            = $request->get('objetivo', $objectUpdate->objetivo);
                $objectUpdate->tipo_cultivo            = $request->get('tipo_cultivo', $objectUpdate->tipo_cultivo);
                $objectUpdate->regresar            = $request->get('regresar', $objectUpdate->regresar);
                $objectUpdate->porque_regresar            = $request->get('porque_regresar', $objectUpdate->porque_regresar);
                $objectUpdate->observaciones            = $request->get('observaciones', $objectUpdate->observaciones);
                $objectUpdate->foto1            = $request->get('foto1', $objectUpdate->foto1);
                $objectUpdate->foto2            = $request->get('foto2', $objectUpdate->foto2);
                $objectUpdate->foto3            = $request->get('foto3', $objectUpdate->foto3);
                $objectUpdate->video            = $request->get('video', $objectUpdate->video);
                $objectUpdate->logrado            = $request->get('logrado', $objectUpdate->logrado);
                $objectUpdate->razon            = $request->get('razon', $objectUpdate->razon);
                $objectUpdate->planificacion            = $request->get('planificacion', $objectUpdate->planificacion);
                $objectUpdate->latitud            = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->longitud            = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->venta_efectiva            = $request->get('venta_efectiva', $objectUpdate->venta_efectiva);
                $objectUpdate->regreso            = $request->get('regreso', $objectUpdate->regreso);
                $objectUpdate->tipo            = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->estado            = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->cliente            = $request->get('cliente', $objectUpdate->cliente);
                $objectUpdate->tipo_visita            = $request->get('tipo_visita', $objectUpdate->tipo_visita);
                $objectUpdate->usuario            = $request->get('usuario', $objectUpdate->usuario);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = FormDiario::find($id);
        if ($objectDelete) {
            try {
                FormDiario::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
