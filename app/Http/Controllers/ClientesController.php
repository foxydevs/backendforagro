<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Clientes;
use App\ClienteUsuario;
use Response;
use Validator;
use DB;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Clientes::all(), 200);
    }

    public function getThisByUser()
    {
        $objectSeeID = ClienteUsuario::select('cliente')->whereRaw('usuario=?',$id)->get();
        if ($objectSeeID) {
            $objectSee = Clientes::whereIn('id',$objectSeeID)->get();
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre'       => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Clientes();
                $newObject->nombre            = $request->get('nombre',null);
                $newObject->apellido          = $request->get('apellido',null);
                $newObject->nit               = $request->get('nit',null);
                $newObject->direccion         = $request->get('direccion',null);
                $newObject->telefono          = $request->get('telefono',null);
                $newObject->celular           = $request->get('celular',null);
                $newObject->latitud           = $request->get('latitud',null);
                $newObject->longitud           = $request->get('longitud',null);
                $newObject->sector           = $request->get('sector',null);
                $newObject->save();
                if($request->get('usuario')){
                    try {
                        $newObject2 = new ClienteUsuario();
                        $newObject2->comprobante            = $request->get('comprobante',1);
                        $newObject2->fecha            = $request->get('fecha',null);
                        $newObject2->estado            = $request->get('estado',2);
                        $newObject2->latitud            = $newObject->latitud;
                        $newObject2->longitud            = $newObject->longitud;
                        $newObject2->tipo            = $request->get('tipo',1);
                        $newObject2->cliente            = $newObject->id;
                        $newObject2->usuario            = $request->get('usuario');
                        $newObject2->save();
                    } catch (Exception $e) {
                        $returnData = array (
                            'status' => 500,
                            'message' => $e->getMessage()
                        );
                        return Response::json($returnData, 500);
                    }
                }
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();
        
        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
                                'south' => '180',
                                'east' => '90',
                                'west' => '270');
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance/$earthRadius;
        foreach ($cardinalCoords as $name => $angle)
        {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
            $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                    'lng' => (float) rad2deg($rLonB));
        }
        return array('min_lat'  => $return['south']['lat'],
                    'max_lat' => $return['north']['lat'],
                    'min_lng' => $return['west']['lng'],
                    'max_lng' => $return['east']['lng']);
    }
    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
            } else {
                return $miles;
            }
    }
    public function clientsNear(Request $request, $id){
        try {
            $lat = $request->get('latitude');
            $lon = $request->get('longitude');
            $distance = $request->get('distance');
            $box = $this->getBoundaries($lat, $lon, $distance);
            
            $objectSeeID = ClienteUsuario::select('cliente')->whereRaw('usuario=?',$id)->get();
            $eventsNear = DB::table('clientes')->select(DB::raw('id,nombre,apellido,direccion,telefono,estado,latitud,longitud, (6371 * ACOS( 
                                            SIN(RADIANS(latitud)) 
                                            * SIN(RADIANS(' . $lat . ')) 
                                            + COS(RADIANS(longitud - ' . $lon . ')) 
                                            * COS(RADIANS(latitud)) 
                                            * COS(RADIANS(' . $lat . '))
                                            )
                               ) AS distance'))->whereRaw('((latitud BETWEEN ? AND ?) AND (longitud BETWEEN ? AND ?)) HAVING distance < ? ', 
                               [$box['min_lat'], $box['max_lat'], $box['min_lng'], $box['max_lng'], $distance])
                               ->whereIn('id',$objectSeeID)->get();
            /*$showEventsNear = collect();
            foreach ($eventsNear as $key => $value) {
                if(strtotime($value->date." ".$value->time) > date('Y-m-d H:m:s')){
                    $objectSee = Events::find($value->id);
                    if ($objectSee) {
                        $objectSee->user;
                        $objectSee->distance = $this->distance($objectSee->latitude,$objectSee->longitud,$lat,$lon,"K");
                        $showEventsNear->push($objectSee);
                    }
                }
            }*/
            
            $eventsId = collect();
            foreach ($eventsNear as $key => $value) {
                $estado = ClienteUsuario::whereRaw('cliente=? and usuario=?',[$value->id,$id])->first();
                $value->estado = $estado->estado;
                $value->usuarioidDelete = $estado->id;
                $eventsId->push($value);
            }


            return Response::json($eventsId, 200);
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }

    public function clientsNearAll(Request $request){
        try {
            $lat = $request->get('latitude');
            $lon = $request->get('longitude');
            // $distance = $request->get('distance');
            $distance = "10000";
            $box = $this->getBoundaries($lat, $lon, $distance);
            
            $objectSeeID = ClienteUsuario::select('cliente')->get();
            $eventsNear = DB::table('clientes')->select(DB::raw('id,nombre,apellido,direccion,telefono,estado,latitud,longitud, (6371 * ACOS( 
                                            SIN(RADIANS(latitud)) 
                                            * SIN(RADIANS(' . $lat . ')) 
                                            + COS(RADIANS(longitud - ' . $lon . ')) 
                                            * COS(RADIANS(latitud)) 
                                            * COS(RADIANS(' . $lat . '))
                                            )
                                            ) AS distance'))->whereRaw('((latitud BETWEEN ? AND ?) AND (longitud BETWEEN ? AND ?)) HAVING distance < ? ', 
                                            [$box['min_lat'], $box['max_lat'], $box['min_lng'], $box['max_lng'], $distance])
                                            ->whereIn('id',$objectSeeID)->get();
            /*$showEventsNear = collect();
            foreach ($eventsNear as $key => $value) {
                if(strtotime($value->date." ".$value->time) > date('Y-m-d H:m:s')){
                    $objectSee = Events::find($value->id);
                    if ($objectSee) {
                        $objectSee->user;
                        $objectSee->distance = $this->distance($objectSee->latitude,$objectSee->longitud,$lat,$lon,"K");
                        $showEventsNear->push($objectSee);
                    }
                }
            }*/
            
            $eventsId = collect();
            foreach ($eventsNear as $key => $value) {
                $estado = ClienteUsuario::select('estado')->whereRaw('cliente=?',[$value->id])->first();
                $value->estado = $estado->estado;
                $value->usuarioidDelete = $estado->id;
                $eventsId->push($value);
            }


            return Response::json($eventsId, 200);
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Clientes::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function find(Request $request)
    {
        $objectSee = Clientes::whereRaw('nit=? || nombre=?',[$request->get('nit'),$request->get('nit')])->first();
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Clientes::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombre            = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->apellido          = $request->get('apellido', $objectUpdate->apellido);
                $objectUpdate->nit               = $request->get('nit', $objectUpdate->nit);
                $objectUpdate->direccion         = $request->get('direccion', $objectUpdate->direccion);
                $objectUpdate->telefono          = $request->get('telefono', $objectUpdate->telefono);
                $objectUpdate->celular           = $request->get('celular', $objectUpdate->celular);
                $objectUpdate->estado            = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->municipio         = $request->get('municipio', $objectUpdate->municipio);
                $objectUpdate->departamento      = $request->get('departamento', $objectUpdate->departamento);
                $objectUpdate->pais              = $request->get('pais', $objectUpdate->pais);
                $objectUpdate->latitud              = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->longitud              = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->sector              = $request->get('sector', $objectUpdate->sector);
                
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Clientes::find($id);
        if ($objectDelete) {
            try {
                Clientes::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
