<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\InventarioCamiones;
use Response;
use Validator;

class InventarioCamionesControlles extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(InventarioCamiones::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = InventarioCamiones::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
                    break;
                }
                case 'type':{
                    $objectSee = InventarioCamiones::whereRaw('user=? and tipo=?',[$id,$state])->with('user')->get();
                    break;
                }
                default:{
                    $objectSee = InventarioCamiones::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = InventarioCamiones::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = InventarioCamiones::where('app','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = InventarioCamiones::where('app','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre'          => 'required',
            'codigo'          => 'required',
            'tipo'            => 'required',
            'precioCosto'     => 'required',
            'precioVenta'     => 'required',
            'cantidad'        => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            $objectUpdate = Productos::whereRaw('codigo=?',[$request->get('codigo')])->first();
            if ($objectUpdate) {
                $returnData = array (
                    'status' => 402,
                    'message' => "El codigo ya existe"
                );
                return Response::json($returnData, 402);
                try {
                    $objectUpdate->nombre      = $request->get('nombre', $objectUpdate->nombre);
                    $objectUpdate->descripcion = $request->get('descripcion', $objectUpdate->descripcion);
                    $objectUpdate->codigo      = $request->get('codigo', $objectUpdate->codigo);
                    $objectUpdate->marcaDes    = $request->get('marcaDes', $objectUpdate->marcaDes);
                    $objectUpdate->tipo        = $request->get('tipo', $objectUpdate->tipo);
            
                    $objectUpdate->save();
                    $objectActualiza = InventarioCamiones::whereRaw('producto=?',[$objectUpdate->id])->first();
                    if ($objectActualiza) {
                        try {
                            $objectActualiza->precioCosto        = $request->get('precioCosto', $objectActualiza->precioCosto);
                            $objectActualiza->precioVenta        = $request->get('precioVenta', $objectActualiza->precioVenta);
                            $objectActualiza->precioClienteEs    = $request->get('precioClienteEs', $objectActualiza->precioClienteEs);
                            $objectActualiza->precioDistribuidor = $request->get('precioDistribuidor', $objectActualiza->precioDistribuidor);
                            $objectActualiza->cantidad           = $request->get('cantidad', $objectActualiza->cantidad);
                            $objectActualiza->minimo             = $request->get('minimo', $objectActualiza->minimo);
                            $objectActualiza->descuento          = $request->get('descuento', $objectActualiza->descuento);
                            $objectActualiza->save();
                            $objectActualiza->productos;
                            return Response::json($objectActualiza, 200);
                        } catch (Exception $e) {
                            $returnData = array (
                                'status' => 500,
                                'message' => $e->getMessage()
                            );
                            return Response::json($returnData, 500);
                        }
                    }
                    else {
                        $returnData = array (
                            'status' => 404,
                            'message' => 'No record found'
                        );
                        return Response::json($returnData, 404);
                    }
                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
            }
            else {
                try {
                    $objectNuevo = new Productos();
                    $objectNuevo->nombre      = $request->get('nombre');
                    $objectNuevo->descripcion = $request->get('descripcion');
                    $objectNuevo->codigo      = $request->get('codigo');
                    $objectNuevo->marcaDes    = $request->get('marcaDes');
                    $objectNuevo->tipo        = $request->get('tipo');
                    $objectNuevo->save();
                        try {
                            $newObject = new InventarioCamiones();
                            $newObject->producto           = $objectNuevo->id;
                            $newObject->precioCosto        = $request->get('precioCosto');
                            $newObject->precioVenta        = $request->get('precioVenta');
                            $newObject->precioClienteEs    = $request->get('precioClienteEs');
                            $newObject->precioDistribuidor = $request->get('precioDistribuidor');
                            $newObject->cantidad           = $request->get('cantidad');
                            $newObject->minimo             = $request->get('minimo');
                            $newObject->descuento          = $request->get('descuento');
                            $newObject->save();
                            $newObject->productos;
                            return Response::json($newObject, 200);
                        } catch (\Illuminate\Database\QueryException $e) {
                            if($e->errorInfo[0] == '01000'){
                                $errorMessage = "Error Constraint";
                            }  else {
                                $errorMessage = $e->getMessage();
                            }
                            $returnData = array (
                                'status' => 505,
                                'SQLState' => $e->errorInfo[0],
                                'message' => $errorMessage
                            );
                            return Response::json($returnData, 500);
                        } catch (Exception $e) {
                            $returnData = array (
                                'status' => 500,
                                'message' => $e->getMessage()
                            );
                            return Response::json($returnData, 500);
                        }
                } catch (\Illuminate\Database\QueryException $e) {
                    if($e->errorInfo[0] == '01000'){
                        $errorMessage = "Error Constraint";
                    }  else {
                        $errorMessage = $e->getMessage();
                    }
                    $returnData = array (
                        'status' => 505,
                        'SQLState' => $e->errorInfo[0],
                        'message' => $errorMessage
                    );
                    return Response::json($returnData, 500);
                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = InventarioCamiones::with('productos')->find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Productos::whereRaw('codigo=? and id!=?',[$request->get('codigo'),$request->get('producto')])->first();
        if ($objectUpdate) {
            $returnData = array (
                'status' => 402,
                'message' => "El codigo ya existe"
            );
            return Response::json($returnData, 402);
        }else{
            $objectUpdate = InventarioCamiones::find($id);
            if ($objectUpdate) {
                try {
                    $objectUpdate->precioCosto        = $request->get('precioCosto', $objectUpdate->precioCosto);
                    $objectUpdate->precioVenta        = $request->get('precioVenta', $objectUpdate->precioVenta);
                    $objectUpdate->precioClienteEs    = $request->get('precioClienteEs', $objectUpdate->precioClienteEs);
                    $objectUpdate->precioDistribuidor = $request->get('precioDistribuidor', $objectUpdate->precioDistribuidor);
                    $objectUpdate->cantidad           = $request->get('cantidad', $objectUpdate->cantidad);
                    $objectUpdate->minimo             = $request->get('minimo', $objectUpdate->minimo);
                    $objectUpdate->descuento          = $request->get('descuento', $objectUpdate->descuento);
                    $objectUpdate->save();
                    $objectActualiza = Productos::find($request->get('producto'));
                    if ($objectActualiza) {
                        try {
                            $objectActualiza->nombre      = $request->get('nombre', $objectActualiza->nombre);
                            $objectActualiza->descripcion = $request->get('descripcion', $objectActualiza->descripcion);
                            $objectActualiza->codigo      = $request->get('codigo', $objectActualiza->codigo);
                            $objectActualiza->marcaDes    = $request->get('marcaDes', $objectActualiza->marcaDes);
                            $objectActualiza->tipo        = $request->get('tipo', $objectActualiza->tipo);
                    
                            $objectActualiza->save();
                            $objectUpdate->productos;
                            return Response::json($objectActualiza, 200);
                        } catch (Exception $e) {
                            $returnData = array (
                                'status' => 500,
                                'message' => $e->getMessage()
                            );
                            return Response::json($returnData, 500);
                        }
                    }
                    else {
                        $returnData = array (
                            'status' => 404,
                            'message' => 'No record found'
                        );
                        return Response::json($returnData, 404);
                    }
                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
            }
            else {
                $returnData = array (
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = InventarioCamiones::find($id);
        if ($objectDelete) {
            try {
                InventarioCamiones::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}