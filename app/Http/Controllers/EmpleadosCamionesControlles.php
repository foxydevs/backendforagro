<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Empleados;
use App\EmpleadosCamiones;
use App\Usuarios;
use Response;
use Validator;

class EmpleadosCamionesControlles extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(EmpleadosCamiones::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = EmpleadosCamiones::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
                    break;
                }
                case 'type':{
                    $objectSee = EmpleadosCamiones::whereRaw('user=? and tipo=?',[$id,$state])->with('user')->get();
                    break;
                }
                default:{
                    $objectSee = EmpleadosCamiones::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = EmpleadosCamiones::whereRaw('user=? and state=?',[$id,$state])->with('user')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = EmpleadosCamiones::where('usuario','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = EmpleadosCamiones::where('camion','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByCamion($id)
    {
        $objectSee = EmpleadosCamiones::select('empleado')->where('camion','=',$id)->get();
        if ($objectSee) {
            $objectSee1 = Usuarios::whereIn('empleado',$objectSee)->get();
    
            return Response::json($objectSee1, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getThisByFree()
    {
        $objectSee = Empleados::select('id')->get();
        if ($objectSee) {
            $objectSee1 = Usuarios::whereIn('empleado',$objectSee)->get();
    
            return Response::json($objectSee1, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'camion'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new EmpleadosCamiones();
                $newObject->nombre            = $request->get('nombre', null);
                $newObject->codigo            = $request->get('codigo', null);
                $newObject->tipo            = $request->get('tipo', null);
                $newObject->estado            = $request->get('estado', null);
                $newObject->latitud            = $request->get('latitud', null);
                $newObject->longitud            = $request->get('longitud', null);
                $newObject->empleado            = $request->get('empleado', null);
                $newObject->camion            = $request->get('camion', null);
                $newObject->usuario            = $request->get('usuario', null);
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    public function storeAll(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'empleados'          => 'required',
            'camion'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                if ( $request->get('empleados') )
                {
                    DB::beginTransaction();
                    $Array = $request->get('empleados');
                    $tutor = $request->get('camion');
                    foreach ($Array as $value)
                    {
                        $existe = EmpleadosCamiones::whereRaw('empleado=? and camion=?',[$value['id'],$tutor])->first();
                        if(!($existe)){   
                            $newObject = new EmpleadosCamiones();
                            $newObject->nombre            = $request->get('nombre',null);
                            $newObject->codigo            = $request->get('codigo',null);
                            $newObject->estado            = $request->get('estado',2);
                            $newObject->tipo            = $request->get('tipo',1);
                            $newObject->empleado            = $value['id'];
                            $newObject->camion            = $tutor;
                            $newObject->save();
                        }
                    }
                    DB::commit();
                    $returnData = array (
                        'status' => 200,
                        'message' => "success"
                    );
                    return Response::json($returnData, 200);
                }

                $returnData = array (
                    'status' => 200,
                    'message' => "success"
                );
                return Response::json($returnData, 200);
                
                
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = EmpleadosCamiones::find($id);
        if ($objectSee) {
           
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = EmpleadosCamiones::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombre            = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->codigo            = $request->get('codigo', $objectUpdate->codigo);
                $objectUpdate->tipo            = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->estado            = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->latitud            = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->longitud            = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->empleado            = $request->get('empleado', $objectUpdate->empleado);
                $objectUpdate->camion            = $request->get('camion', $objectUpdate->camion);
                $objectUpdate->usuario            = $request->get('usuario', $objectUpdate->usuario);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = EmpleadosCamiones::find($id);
        if ($objectDelete) {
            try {
                EmpleadosCamiones::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
