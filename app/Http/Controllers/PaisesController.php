<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Paises;
use Response;
use Validator;

class PaisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Paises::all(), 200);
    }

    
    public function getThisByFilter(Request $request, $state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Paises::whereRaw('estado=?',$request->get('data'))->get();
                    break;
                }
                case 'type':{
                    $objectSee = Paises::whereRaw('tipo=?',$request->get('data'))->get();
                    break;
                }
                case 'countryCode':{
                    $objectSee = Paises::whereRaw('countryCode=?',$request->get('data'))->get();
                    break;
                }
                case 'countryName':{
                    $objectSee = Paises::whereRaw('countryName=?',$request->get('data'))->get();
                    break;
                }
                case 'postalCode':{
                    $objectSee = Paises::whereRaw('postalCode=?',$request->get('data'))->get();
                    break;
                }
                case 'administrativeArea':{
                    $objectSee = Paises::whereRaw('administrativeArea=?',$request->get('data'))->get();
                    break;
                }
                case 'subAdministrativeArea':{
                    $objectSee = Paises::whereRaw('subAdministrativeArea=?',$request->get('data'))->get();
                    break;
                }
                case 'locality':{
                    $objectSee = Paises::whereRaw('locality=?',$request->get('data'))->get();
                    break;
                }
                case 'subLocality':{
                    $objectSee = Paises::whereRaw('subLocality=?',$request->get('data'))->get();
                    break;
                }
                case 'thoroughfare':{
                    $objectSee = Paises::whereRaw('thoroughfare=?',$request->get('data'))->get();
                    break;
                }
                case 'subThoroughfare':{
                    $objectSee = Paises::whereRaw('subThoroughfare=?',$request->get('data'))->get();
                    break;
                }
                default:{
                    $objectSee = Paises::whereRaw('estado=?',$request->get('data'))->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Paises::whereRaw('estado=?',$request->get('data'))->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Paises();
                $newObject->nombre            = $request->get('nombre',null);
                $newObject->codigo            = $request->get('codigo',null);
                $newObject->LADA            = $request->get('LADA',null);
                $newObject->cod            = $request->get('cod',null);
                $newObject->estado            = $request->get('estado',null);
                $newObject->countryCode     = $request->get('countryCode',null);
                $newObject->countryName     = $request->get('countryName',null);
                $newObject->postalCode     = $request->get('postalCode',null);
                $newObject->administrativeArea     = $request->get('administrativeArea',null);
                $newObject->subAdministrativeArea     = $request->get('subAdministrativeArea',null);
                $newObject->locality     = $request->get('locality',null);
                $newObject->subLocality     = $request->get('subLocality',null);
                $newObject->thoroughfare     = $request->get('thoroughfare',null);
                $newObject->subThoroughfare     = $request->get('subThoroughfare',null);
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Paises::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Paises::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombre            = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->codigo            = $request->get('codigo', $objectUpdate->codigo);
                $objectUpdate->LADA            = $request->get('LADA', $objectUpdate->LADA);
                $objectUpdate->cod            = $request->get('cod', $objectUpdate->cod);
                $objectUpdate->estado            = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->countryCode     = $request->get('countryCode', $objectUpdate->countryCode);
                $objectUpdate->countryName     = $request->get('countryName', $objectUpdate->countryName);
                $objectUpdate->postalCode     = $request->get('postalCode', $objectUpdate->postalCode);
                $objectUpdate->administrativeArea     = $request->get('administrativeArea', $objectUpdate->administrativeArea);
                $objectUpdate->subAdministrativeArea     = $request->get('subAdministrativeArea', $objectUpdate->subAdministrativeArea);
                $objectUpdate->locality     = $request->get('locality', $objectUpdate->locality);
                $objectUpdate->subLocality     = $request->get('subLocality', $objectUpdate->subLocality);
                $objectUpdate->thoroughfare     = $request->get('thoroughfare', $objectUpdate->thoroughfare);
                $objectUpdate->subThoroughfare     = $request->get('subThoroughfare', $objectUpdate->subThoroughfare);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Paises::find($id);
        if ($objectDelete) {
            try {
                Paises::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}