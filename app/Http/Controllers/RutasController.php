<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Rutas;
use App\Clientes;
use Response;
use DB;
use Validator;

class RutasController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Rutas::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Rutas::whereRaw('usuario=? and estado=?',[$id,$state])->with('users')->get();
                    break;
                }
                case 'type':{
                    $objectSee = Rutas::whereRaw('usuario=? and tipo=?',[$id,$state])->with('users')->get();
                    break;
                }
                default:{
                    $objectSee = Rutas::whereRaw('usuario=? and estado=?',[$id,$state])->with('users')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Rutas::whereRaw('usuario=? and estado=?',[$id,$state])->with('users')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = Rutas::select('clientes_usuario')->where('usuario','=',$id)->get();
        if ($objectSee) {
            $objectSee1 = Clientes::whereIn('id',$objectSee)->get();
    
            return Response::json($objectSee1, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getThisByFree()
    {
        $objectSee = Rutas::select('clientes_usuario')->get();
        if ($objectSee) {
            $objectSee1 = Clientes::whereNotIn('id',$objectSee)->get();
    
            return Response::json($objectSee1, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = Rutas::where('clientes_usuario','=',$id)->with('users')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clientes_usuario'          => 'required',
            'usuario'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                if ( $request->get('clientes_usuario') )
                {
                    DB::beginTransaction();
                    $Array = $request->get('clientes_usuario');
                    $tutor = $request->get('usuario');
                    foreach ($Array as $value)
                    {
                        $existe = Rutas::whereRaw('clientes_usuario=? and usuario=?',[$value['id'],$tutor])->first();
                        if(!($existe)){   
                            $objectSee = Clientes::find($value['id']);
                            if ($objectSee) {
                                $newObject = new Rutas();
                                $newObject->comprobante            = $request->get('comprobante',1);
                                $newObject->fecha            = $request->get('fecha',null);
                                $newObject->estado            = $request->get('estado',2);
                                $newObject->latitud_pre            = $objectSee->latitud_pre;
                                $newObject->longitud_pre            = $objectSee->longitud_pre;
                                $newObject->latitud_fin            = $objectSee->latitud_fin;
                                $newObject->longitud_fin            = $objectSee->longitud_fin;
                                $newObject->tipo            = $request->get('tipo',1);
                                $newObject->clientes_usuario            = $value['id'];
                                $newObject->usuario            = $tutor;
                                $newObject->save();
                            }
                            else {
                                $returnData = array (
                                    'status' => 404,
                                    'message' => 'No record found'
                                );
                                return Response::json($returnData, 404);
                            } 
                            
                        }
                    }
                    DB::commit();
                    $returnData = array (
                        'status' => 200,
                        'message' => "success"
                    );
                    return Response::json($returnData, 200);
                }

                $returnData = array (
                    'status' => 200,
                    'message' => "success"
                );
                return Response::json($returnData, 200);
                
                
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Rutas::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    public function removeClientes(Request $request)
    {
        try
        {
            if ( $request->get('clientes_usuario') )
            {
                DB::beginTransaction();
                $Array = $request->get('clientes_usuario');
                $tutor = $request->get('usuario');
                $studentsId = collect();
                foreach ($Array as $value)
                {
                    $objectDelete = Rutas::whereRaw('clientes_usuario=? and usuario=?',[$value['id'],$tutor])->first();
                    if(($objectDelete)){    
                        $studentsId->push($objectDelete->id); 
                        Rutas::destroy($objectDelete->id);      
                    } 
                }

                DB::commit();
                $returnData = array (
                    'status' => 200,
                    'message' => "success"
                );
                return Response::json($returnData, 200);
            }
            $returnData = array (
                'status' => 200,
                'message' => "success"
            );
            return Response::json($returnData, 200);  
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            if($e->errorInfo[0] == '01000'){
                $errorMessage = "Error Constraint";
            }  else {
                $errorMessage = $e->getMessage();
            }
            $returnData = array (
                'status' => 505,
                'SQLState' => $e->errorInfo[0],
                'message' => $errorMessage
            );
            return Response::json($returnData, 500);
        }
        catch (Exception $e)
        {
            DB::rollback();
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Rutas::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->comprobante            = $request->get('comprobante', $objectUpdate->comprobante);
                $objectUpdate->fecha            = $request->get('fecha', $objectUpdate->fecha);
                $objectUpdate->estado            = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->latitud_pre            = $request->get('latitud_pre', $objectUpdate->latitud_pre);
                $objectUpdate->longitud_pre            = $request->get('longitud_pre', $objectUpdate->longitud_pre);
                $objectUpdate->latitud_fin            = $request->get('latitud_fin', $objectUpdate->latitud_fin);
                $objectUpdate->longitud_fin            = $request->get('longitud_fin', $objectUpdate->longitud_fin);
                $objectUpdate->tipo            = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->clientes_usuario            = $request->get('clientes_usuario', $objectUpdate->clientes_usuario);
                $objectUpdate->usuario            = $request->get('usuario', $objectUpdate->usuario);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Rutas::find($id);
        if ($objectDelete) {
            try {
                Rutas::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
