<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Ubicaciones;
use Response;
use Validator;

class UbicacionesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Ubicaciones::with('usuarios','camiones','empleados_camiones')->get(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Ubicaciones::whereRaw('state=?',[$id,$state])->with('usuarios','camiones','empleados_camiones')->get();
                    break;
                }
                case 'type':{
                    $objectSee = Ubicaciones::whereRaw('tipo=?',[$id,$state])->with('usuarios','camiones','empleados_camiones')->get();
                    break;
                }
                case 'camion':{
                    $objectSee = Ubicaciones::whereRaw('camion=?',[$id,$state])->with('usuarios','camiones','empleados_camiones')->get();
                    break;
                }
                case 'empleados_camion':{
                    $objectSee = Ubicaciones::whereRaw('empleados_camion=?',[$id,$state])->with('usuarios','camiones','empleados_camiones')->get();
                    break;
                }
                default:{
                    $objectSee = Ubicaciones::whereRaw('state=?',[$id,$state])->with('usuarios','camiones','empleados_camiones')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Ubicaciones::whereRaw('state=?',[$id,$state])->with('usuarios','camiones','empleados_camiones')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByUser($id)
    {
        $objectSee = Ubicaciones::where('usuario','=',$id)->with('usuarios','camiones','empleados_camiones')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function getThisByClient($id)
    {
        $objectSee = Ubicaciones::where('app','=',$id)->with('usuarios','camiones','empleados_camiones')->get();
        if ($objectSee) {
    
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude'          => 'required',
            'longitude'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Ubicaciones();
                $newObject->fecha            = $request->get('fecha', null);
                $newObject->hora            = $request->get('hora', null);
                $newObject->place_id            = $request->get('place_id', null);
                $newObject->place            = $request->get('place', null);
                $newObject->direccion            = $request->get('direccion', null);
                $newObject->picture            = $request->get('picture', null);
                $newObject->description            = $request->get('description', null);
                $newObject->latitude            = $request->get('latitude', null);
                $newObject->longitude            = $request->get('longitude', null);
                $newObject->tipo            = $request->get('tipo', null);
                $newObject->state            = $request->get('state', null);
                $newObject->usuario            = $request->get('usuario', null);
                $newObject->camion            = $request->get('camion', null);
                $newObject->empleados_camion            = $request->get('empleados_camion', null);
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    public function uploadAvatar(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'avatar'      => 'required|image|mimes:jpeg,png,jpg'
        ]);
    
        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
    
                $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);
    
                $objectUpdate->picture = Storage::disk('s3')->url($path);
                $objectUpdate->save();
    
                return Response::json($objectUpdate, 200);
    
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
    
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Ubicaciones::whereRaw('id=?',$id)->with('usuarios','camiones','empleados_camiones')->first();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Ubicaciones::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->fecha            = $request->get('fecha', $objectUpdate->fecha);
                $objectUpdate->hora            = $request->get('hora', $objectUpdate->hora);
                $objectUpdate->place_id            = $request->get('place_id', $objectUpdate->place_id);
                $objectUpdate->place            = $request->get('place', $objectUpdate->place);
                $objectUpdate->direccion            = $request->get('direccion', $objectUpdate->direccion);
                $objectUpdate->picture            = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->description            = $request->get('description', $objectUpdate->description);
                $objectUpdate->latitude            = $request->get('latitude', $objectUpdate->latitude);
                $objectUpdate->longitude            = $request->get('longitude', $objectUpdate->longitude);
                $objectUpdate->tipo            = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->usuario            = $request->get('usuario', $objectUpdate->usuario);
                $objectUpdate->camion            = $request->get('camion', $objectUpdate->camion);
                $objectUpdate->empleados_camion            = $request->get('empleados_camion', $objectUpdate->empleados_camion);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Ubicaciones::find($id);
        if ($objectDelete) {
            try {
                Ubicaciones::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
