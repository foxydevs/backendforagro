<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoVisita extends Model
{
    protected $table = 'tipo_visita';
}
