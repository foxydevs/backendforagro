<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormVisita extends Model
{
    protected $table = 'form_visita';

    public function clientes(){
        return $this->hasOne('App\Clientes','id','cliente');
    }

    public function tiposCliente(){
        return $this->hasOne('App\TipoCliente','id','tipo_cliente');
    }

    public function tiposAvance(){
        return $this->hasOne('App\TipoAvance','id','tipo_avance');
    }

    public function objetivo(){
        return $this->hasOne('App\Objetivos','id','objetivos');
    }

    public function cultivos(){
        return $this->hasOne('App\Cultivos','id','cultivo');
    }

    public function users(){
        return $this->hasOne('App\Usuarios','id','usuario')->with('empleados');
    }

    public function resultados(){
        return $this->hasMany('App\FormVisita','planificacion','id');
    }
}
