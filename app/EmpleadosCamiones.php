<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpleadosCamiones extends Model
{
    protected $table = 'empleados_camiones';

    public function camiones(){
        return $this->hasOne('App\Camiones','id','camion');
    }
}
