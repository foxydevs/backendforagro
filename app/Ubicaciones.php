<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubicaciones extends Model
{
    protected $table = 'ubicaciones';

    public function usuarios(){
        return $this->hasOne('App\Usuarios','id','usuario');
    }

    public function camiones(){
        return $this->hasOne('App\Camiones','id','camion');
    }

    public function empleados_camiones(){
        return $this->hasOne('App\EmpleadosCamiones','id','empleados_camion');
    }
}
 