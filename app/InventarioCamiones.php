<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventarioCamiones extends Model
{
    protected $table = 'inventario_camiones';
}
