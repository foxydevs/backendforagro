<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Proveedores extends Model
{
    // use SoftDeletes;
    protected $table = 'proveedores';

    public function sectores(){
        return $this->hasOne('App\Sectores','id','sector');
    }
}
