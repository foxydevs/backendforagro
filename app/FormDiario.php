<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormDiario extends Model
{
    protected $table = 'form_diario';

    public function clientes(){
        return $this->hasOne('App\Clientes','id','cliente');
    }

    public function tipos(){
        return $this->hasOne('App\TipoVisita','id','tipo_visita');
    }

    public function objetivos(){
        return $this->hasOne('App\Objetivos','id','objetivo');
    }

    public function cultivos(){
        return $this->hasOne('App\Cultivos','id','tipo_cultivo');
    }

    public function users(){
        return $this->hasOne('App\Usuarios','id','usuario');
    }

    public function resultados(){
        return $this->hasMany('App\FormDiario','planificacion','id');
    }
}
