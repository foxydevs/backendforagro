<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sectores extends Model
{
    protected $table = 'sector';

    public function paises(){
        return $this->hasOne('App\Paises','id','pais');
    }

}
